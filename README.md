
# Table of Contents

1.  [py-alarm-sink - NSO Python alarm sink](#orgd32e789)
2.  [License](#orgd47ce45)


<a id="orgd32e789"></a>

# py-alarm-sink - NSO Python alarm sink

This NSO package helps you create alarms in NSO from Python code!

The package contains a Python module `alarm_sink` which allows for simple creation and updating of alarms within NSO. The alarm list in NSO (`/al:alarms/alarm-list/alarm`) is writable using the existing APIs (maagic). In fact, that is what this package does under the hood! To use the package in your own code, declare this package as a dependency of your package in `package-meta-data.xml`. For example, in this repository we have a test package `test-packages/test-alarm-sink` which uses `py-alarm-sink` in an NSO action. This is the relevant section of `package-meta-data.xml`:

    <required-package>
      <name>py-alarm-sink</name>
    </required-package>

In the package code, just `import alarm_sink` and use the API:

    from alarm_sink import alarm_sink
    
    # First create the AlarmId tuple. This is used as the key for the alarm list entry
    alarm_id = alarm_sink.AlarmId('some-device', '/devices/device[name="some-device"]', 'custom-alarm', 'specific-reason')
    # Then craete the Alarm object. This object contains the current state of the
    # alarm - severity, text, and cleared status. If the same object already exist
    # in the list of alarm state changes, nothing is done. If an attribute of an
    # alarm was changed, the data will be flushed when calling submit_alarm.
    sev = alarm_sink.PerceivedSeverity.MINOR
    alarm = alarm_sink.Alarm(alarm_id, severity=sev, alarm_text='A minor problem occured on the kajigger')
    # Flush the change to CDB. Without parameters, the AlarmSink starts its own
    # MAAPI session and transaction. It is also possible to pass an existing MAAPI
    # session.
    with alarm_sink.AlarmSink() as ask:
        ask.submit_alarm(alarm)
    
    # Some time later, we can clear the alarm. Here we use the same `submit_alarm()` method
    # which means we must provide a valid Alarm object.
    alarm.cleared = True
    alarm.alarm_text = 'All is normal on the kajigger'
    with alarm_sink.AlarmSink() as ask:
        ask.submit_alarm(alarm)
    
    
    # If you really just want to clear the alarm you can also use the `clear_alarm()` method.
    with alarm_sink.AlarmSink() as ask:
        ask.clear_alarm(alarm_id, alarm_text='All is normal on the kajigger')

For more options I encourage you to read the source and comments in the `alarm_sink` module itself.


<a id="orgd47ce45"></a>

# License

Copyright 2020 Deutsche Telekom AG

Licensed under the Apache License, Version 2.0 (the &ldquo;License&rdquo;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &ldquo;AS IS&rdquo; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

